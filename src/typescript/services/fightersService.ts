import { callApi } from '../helpers/apiHelper';
import { IFighter, IFighterDetails } from '../interface/fighterInterface';

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi <IFighter[]>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighterDetails> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi<IFighterDetails>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
