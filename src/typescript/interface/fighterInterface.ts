export interface IFighter {
  _id: string;
  name: string;
  source: string;
}
export interface IFighterDetails {
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
}

export interface plaerInterface extends IFighterDetails {
  startHelth: number;
  playerHelthBar: HTMLElement | any;
  onBlock: boolean;
  criticalHitTime: number;

  gethealthIndicator(): number;

  getCriticalHit(): number;

  setHealth(damage: number): void;

  setBlock(block: boolean): void;

  setCriticalHitTime(): void;

  setHelhtBar(): void;
}
