export interface IcreateElement {
  tagName: string;
  className?: string;
  attributes?: any
}

export interface IcreateModal {
  title: string;
  bodyElement: string;
  onClose: Function
}
