export type PlayerIndicator = 'left-fighter-indicator' | 'right-fighter-indicator';
export type Position = 'left' | 'right';
export type winnerType = {
  title: string;
  bodyElement: string;
  onClose: Function;
};
export type controlsType = {
  PlayerOneAttack: string;
  PlayerOneBlock: string;
  PlayerTwoAttack: string;
  PlayerTwoBlock: string;
  PlayerOneCriticalHitCombination: Array<string>;
  PlayerTwoCriticalHitCombination: Array<string>;
};
