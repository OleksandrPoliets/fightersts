import { createElement } from '../helpers/domHelper';
import { IFighterDetails } from '../interface/fighterInterface';
import { Position } from '../typs/typs'

export function createFighterPreview(fighter: IFighterDetails, position: Position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  function createFighterDescription(fighter: IFighterDetails): HTMLElement{
    const {name} =  fighter;
    const fighterCard = createElement({
      tagName: 'div',
      className: 'fighter-preview_card',
    });
    const fighterName = createElement({
      tagName: 'h2',
      className: 'fighter-card-name',
    });
    const fighterSpecificationsWrap = createElement({
      tagName: 'div',
      className: 'fighter-card-specification-wrap',
    });

    Object.entries(fighter).forEach(([key, value]) => {
      if(key !== '_id' && key !== 'source' && key !== 'name'){
        fighterSpecificationsWrap.appendChild(fighterSpecification(key, value));
      }
    });

    fighterName.innerText = name;
    fighterCard.appendChild(fighterName);
    fighterCard.appendChild(fighterSpecificationsWrap);

    return fighterCard;
  }

  function fighterSpecification(header: string, info: string): HTMLElement{
    const fighterParameterhWrap = createElement({
      tagName: 'div',
      className: 'fighter-card-specification',
    });
    const fighterParameterHeader = createElement({
      tagName: 'div',
      className: 'fighter-card-specification-header',
    });
    const fighterParameterInfo = createElement({
      tagName: 'div',
      className: 'fighter-card-specification-info',
    });

    fighterParameterHeader.innerText = header[0].toUpperCase() + header.slice(1);
    fighterParameterInfo.innerText = info;
    fighterParameterhWrap.appendChild(fighterParameterHeader);
    fighterParameterhWrap.appendChild(fighterParameterInfo);

    return fighterParameterhWrap;
  }

  if(fighter){
    const fighterImg = createFighterImage(fighter);

    if(position === 'right'){
      fighterImg.style.transform = 'scale(-1, 1)';
    }

    fighterElement.appendChild(fighterImg);
    fighterElement.appendChild(createFighterDescription(fighter));
  }

  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
