import { controls } from '../../constants/controls';
import { IFighterDetails, plaerInterface } from '../interface/fighterInterface';
import { PlayerIndicator } from '../typs/typs'


export async function fight([firstFighter, secondFighter]: Array<IFighterDetails>): Promise<IFighterDetails> {
  return new Promise((resolve) => {
    const keyCombination = new Set();

    const playerOne: plaerInterface = generetPlayer(firstFighter, 'left-fighter-indicator');
    const playerTwo: plaerInterface = generetPlayer(secondFighter, 'right-fighter-indicator');

    const keyUp = (e: KeyboardEvent) => {
      keyCombination.clear();
      switch (e.code) {
        case controls.PlayerOneBlock:
          playerOne.setBlock(false);
          break;

        case controls.PlayerTwoBlock:
          playerTwo.setBlock(false);
          break;
      }
    };

    const keyDown = (e: KeyboardEvent) => {
      switch (e.code) {
        case controls.PlayerOneAttack:
          if (!e.repeat) {
            makeHit(playerOne, playerTwo, resolve);
          }
          break;

        case controls.PlayerOneBlock:
          playerOne.setBlock(true);
          break;

        case controls.PlayerTwoAttack:
          if (!e.repeat) {
            makeHit(playerTwo, playerOne, resolve);
          }
          break;

        case controls.PlayerTwoBlock:
          playerTwo.setBlock(true);
          break;

        default:
          keyCombination.add(e.code);

          if (keyCombination.size > 2) {
            const playerOneCrit = controls.PlayerOneCriticalHitCombination.every(el => keyCombination.has(el));
            const playerTwoCrit = controls.PlayerTwoCriticalHitCombination.every(el => keyCombination.has(el));

            if (playerOneCrit) {
              criticalHit(playerOne, playerTwo, resolve);
            }
            if (playerTwoCrit) {
              criticalHit(playerTwo, playerOne, resolve);
            }
          }
          break;
      }
    };

    document.addEventListener('keydown', keyDown);
    document.addEventListener('keyup', keyUp);
  });
}

function generetPlayer(fighter: IFighterDetails, indicator: PlayerIndicator): plaerInterface {
  const timeOut: number = 10000;
  const tempPlayer: plaerInterface = {
    ...fighter,
    criticalHitTime: Date.now() - timeOut,
    onBlock: false,
    gethealthIndicator(): number {
      return this.health * 100 / this.startHelth;
    },
    getCriticalHit() {
      return this.attack * 2;
    },
    setHealth(damage) {
      this.health = this.health - damage < 0 ? 0 : this.health - damage;
    },
    setBlock(block) {
      this.onBlock = block;
    },
    setCriticalHitTime() {
      this.criticalHitTime = Date.now();
    },
    setHelhtBar() {
      this.playerHelthBar.style.width = `${this.gethealthIndicator()}%`;
    },
    startHelth: fighter.health,
    playerHelthBar: document.getElementById(indicator)
  };

  return tempPlayer;
}

const findWiner = (attacker: plaerInterface, defender: plaerInterface, endFight: Function): void => {
  if (defender.health === 0) {
    endFight(attacker);
  }
};

const makeHit = (attacker: plaerInterface, defender: plaerInterface, resolve: Function): void => {
  if (!defender.onBlock && !attacker.onBlock) {
    defender.setHealth(getDamage(attacker, defender));
    defender.setHelhtBar();
    findWiner(attacker, defender, resolve);
  }
};

const criticalHit = (attacker: plaerInterface, defender: plaerInterface, resolve: Function): void => {
  const time: number = Date.now();
  const timeOut: number = 10000;

  if (time - attacker.criticalHitTime > timeOut) {
    defender.setHealth(attacker.getCriticalHit());
    defender.setHelhtBar();
    attacker.setCriticalHitTime();
    findWiner(attacker, defender, resolve);
  }
};

export function getDamage(attacker: plaerInterface, defender: plaerInterface): number {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter: plaerInterface): number {
  const { attack } = fighter;
  const criticalHitPower = Math.random() + 1;

  return attack * criticalHitPower;
}

export function getBlockPower(fighter: plaerInterface): number {
  const { defense } = fighter;
  const blockPower = Math.random() + 1;

  return defense * blockPower;
}
