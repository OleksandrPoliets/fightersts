import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighterDetails } from '../interface/fighterInterface';

export function createFightersSelector(): Function {
  let selectedFighters: Array<IFighterDetails> = [];

  return async (event: MouseEvent, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  if(!fighterDetailsMap.has(fighterId)){
    try {
      fighterDetailsMap.set(fighterId, await fighterService.getFighterDetails(fighterId));
    } catch (error) {
      throw error;
    }
  }

  return fighterDetailsMap.get(fighterId);
}

function renderSelectedFighters(selectedFighters: Array<IFighterDetails>) {
  const fightersPreview = document.querySelector('.preview-container___root') as HTMLElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: '../../../resources/versus.png' },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Array<IFighterDetails>) {
  renderArena(selectedFighters);
}
