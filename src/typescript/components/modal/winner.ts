import {showModal} from './modal';
import { IFighterDetails } from '../../interface/fighterInterface';
import { winnerType } from '../../typs/typs';

export function showWinnerModal(fighter: IFighterDetails): void {
  const winner: winnerType = {
    title: 'You rock!',
    bodyElement: fighter.name,
    onClose: () => {window.location.reload()}
  }

  showModal(winner);
}
